# USB device driver
The driver has been created based on `stack_chardev_module` project ([gitlab](https://gitlab.com/AsiiaPine/alup/-/tree/master/stack_chardev)).
## Description:
The driver creates a stack which can be accessed only with an electronic key plugged into your device, so it allows to guard sensitive data from unlucky blackhead hacker.
The driver works with a specific device - an electronic key, which must be specified in the following form:
Put vendor_id and product_id values into the lines 16 and 17 in the stack_chardev.c file.
```
#define VENDOR_ID 0x<your_device_vendor_ID>
#define PRODUCT_ID 0x<your_device_product_ID>
```

## Code description:
- **device_open()**
Calls `try_module_get` to increment the module's usage counter.
- **device_read()**
When a thread calls the function, it locks the mutex exclusively for this task. If the mutex is not available right now, the thread will sleep until it can get it.
The function checks if the `copy_to_user` run successfully, pops the elements from the stack and copy them into process' buffer, otherwise some data will be missed during the operation, so the `ENOMEM` error has been added.
- **device_write()**
The function writes the data passed by a thread and checks if the stack can manage the data, if not, the `ENOSPC` is raised.
While the buf data is written into the stack, it splits the buf into 32 bits segments and pop them one by one. 
The write function guards the stack with `mutex_lock` and does not allow other threads to change the data of the stack during the operation.
- **device_release()**
The function calls `module_put`, which decrement the usage count of the module, when the value decrease to zero, the kernel may safely unload the `chardev` module.
- **device_ioctl(): An optional function for device configuration.***
The function is called when a process calls `IOCTL` command to device file. The `device_ioctl` changes the stack size to value passed with the command.
- **create_dev()**
Checks if device already created, otherwise calls `device_create()`
```
static void create_dev(void)
{
  if (chardev_created)
    // printk(KERN_INFO "chardev device exists\n");
    return;
  chardev_device = device_create(chardev_class, NULL, MKDEV(Major, 0), NULL,
                                 "stack_chardev");
  chardev_created = true;
}
```
- **destroy_dev()**
Checks if chardev exists and destroys it, otherwise do nothing
```
static void destroy_dev(void)
{
  if (!chardev_created)
    return;
  device_destroy(chardev_class, MKDEV(Major, 0));
  chardev_created = false;
}
```
- **device_connect()**
Callback used when the USB device matching `vendor` and `product` ids is connected.

```
static int device_connect(struct usb_interface *interface,
                          const struct usb_device_id *id)
{
  printk(KERN_INFO "stack_chardev: USB device connected: %04X:%04X\n", id->idVendor, id->idProduct);
  create_dev();
  my_usb_device = interface_to_usbdev(interface);
  device_present = true;
  return 0;
}
```

- **device_disconnect()**
Callback used when the USB device matching `vendor` and `product` ids is unplugged.
```
static void device_disconnect(struct usb_interface *interface)
{
  printk(KERN_INFO "stack_chardev: USB device disconnected\n");
  destroy_dev();
  printk(KERN_INFO "stack_chardev: chardev device destroyed\n");

  my_usb_device = NULL;
  device_present = false;
}
```

- **chardev_init()**
Module initialization function
- **chardev_exit()**
Callback used when a module removes from kernel.

# The Test: 
- The key test:
1 - the module has been inserted into kernel, but no electronic key plugged
2 - key has been plugged, "Hello world" string is pushed into the stack
3 - key unplugged, device destroyed
4 - key has been returned, the stack data is available on the device.
Console workflow:
![Console output](pictures/key_test_console_output.png)
kernel messages:
<!-- ![](pictures/key_test.png) -->
![dmesg output](pictures/key_test_dmesg.png)
- Test of access to the stack_chardev device, after the key is unplugged
![](pictures/no_key_stack_access_test.png)