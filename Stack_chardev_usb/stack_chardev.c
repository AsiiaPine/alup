#include <linux/device.h>
#include <linux/fs.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/usb.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Asiiapine");
MODULE_DESCRIPTION("A Simple chardev device implementing stack");

#define DEVICE_NAME "stack_chardev"
#define BUFFER_SIZE 1024
#define CHANGE_STACK_SIZE 1
#define VENDOR_ID 0x8644
#define PRODUCT_ID 0x800b

static int Major;
static struct class *chardev_class = NULL;
static struct device *chardev_device = NULL;
static struct usb_device *my_usb_device = NULL;
struct usb_interface *intf;

typedef struct
{
  int32_t value;
} STACK;

static STACK *stack;
static ssize_t current_size = BUFFER_SIZE;
static ssize_t current_pos = -1;
static struct mutex my_mutex;
static bool device_present = false;
static bool chardev_created = false;

// Calls `try_module_get` to increment the module's usage counter.
static int device_open(struct inode *inode, struct file *file)
{
  printk(KERN_INFO "stack_chardev: device_open\n");
  try_module_get(THIS_MODULE);
  return 0;
}

// When a thread calls the function, it locks the mutex exclusively for this task. If the mutex is not available right now, the thread will sleep until it can get it.
// The function checks if the `copy_to_user` run successfully, pops the elements from the stack and copy them into process' buffer, otherwise some data will be missed during the operation, so the `ENOMEM` error has been added.
static ssize_t device_read(struct file *filp, char *buffer, size_t length,
                           loff_t *offset)
{
  int bytes_read = 0;

  mutex_lock(&my_mutex);

  printk(KERN_INFO "stack_chardev: device_read\n");

  while (length >= sizeof(STACK) && current_pos >= 0)
  {
    if (copy_to_user(buffer, &stack[current_pos--], sizeof(STACK)))
    {
      mutex_unlock(&my_mutex);
      return -EFAULT;
    }
    length -= sizeof(STACK);
    buffer += sizeof(STACK);
    bytes_read += sizeof(STACK);
  }

  mutex_unlock(&my_mutex);

  return bytes_read;
}

// The function writes the data passed by a thread and checks if the stack can manage the data, if not, the `ENOSPC` is raised.
// While the buf data is written into the stack, it splits the buf into 32 bits segments and pop them one by one. 
// The write function guards the stack with `mutex_lock` and does not allow other threads to change the data of the stack during the operation.
static ssize_t device_write(struct file *filp, const char *buf, size_t len,
                            loff_t *offset)
{
  int bytes_written = 0;

  printk(KERN_INFO "stack_chardev: device_write\n");

  mutex_lock(&my_mutex);

  if ((current_pos + 1) * sizeof(STACK) >= current_size)
  {
    printk(KERN_ALERT "stack_chardev: Stack is full\n");
    mutex_unlock(&my_mutex);
    return -ENOSPC;
  }

  while (len >= sizeof(STACK) &&
         (current_pos + 1) * sizeof(STACK) < current_size)
  {
    if (copy_from_user(&stack[++current_pos], buf, sizeof(STACK)))
    {
      mutex_unlock(&my_mutex);
      return -ENOSPC;
    }
    buf += sizeof(STACK);
    len -= sizeof(STACK);
    bytes_written += sizeof(STACK);
  }

  mutex_unlock(&my_mutex);

  printk(KERN_INFO "stack_chardev: Written %d bytes to stack\n", bytes_written);
  return bytes_written;
}

// The function calls `module_put`, which decrement the usage count of the module, 
// when the value decrease to zero, the kernel may safely unload the `chardev` module.
static int device_release(struct inode *inode, struct file *filp)
{
  printk(KERN_INFO "stack_chardev: device_release\n");
  module_put(THIS_MODULE);
  return 0;
}

static long resize_stack(long new_size)
{
  STACK *new_stack;
  ssize_t size_in_bytes;
  ssize_t bytes_to_copy;

  if (new_size <= 0 || new_size > 1024 * 1024 * 512)
    return -EINVAL;

  printk(KERN_INFO "stack_chardev: resizing stack to %ld bytes\n", new_size);

  mutex_lock(&my_mutex);
  new_stack = kvmalloc(new_size * sizeof(STACK), GFP_KERNEL);
  size_in_bytes = new_size * sizeof(STACK);
  bytes_to_copy = min(size_in_bytes, (ssize_t)(current_size * sizeof(STACK)));
  memcpy(new_stack, stack, bytes_to_copy);
  kfree(stack);
  stack = new_stack;
  current_size = new_size;
  if (current_pos > current_size - 1)
    current_pos = current_size - 1;
  mutex_unlock(&my_mutex);
  return 0;
}

// The function is called when a process calls `IOCTL` command to device file. The `device_ioctl` changes the stack size to value passed with the command.
static long device_ioctl(struct file *filp, unsigned int cmd,
                         unsigned long arg)
{
  printk(KERN_INFO "stack_chardev: device_ioctl\n");

  switch (cmd)
  {
  case CHANGE_STACK_SIZE:
    return resize_stack((long)arg);
  }

  return 0;
}

static const struct file_operations fops = {.owner = THIS_MODULE,
                                            .open = device_open,
                                            .release = device_release,
                                            .write = device_write,
                                            .read = device_read,
                                            .unlocked_ioctl = device_ioctl};

// Checks if device already created, otherwise calls `device_create()`
static void create_dev(void)
{
  if (chardev_created)
    // printk(KERN_INFO "chardev device exists\n");
    return;
  chardev_device = device_create(chardev_class, NULL, MKDEV(Major, 0), NULL,
                                 "stack_chardev");
  chardev_created = true;
}

 // Checks if chardev exists and destroys it, otherwise do nothing
static void destroy_dev(void)
{
  if (!chardev_created)
      // printk(KERN_INFO "chardev device does not exist\n");
    return;
  device_destroy(chardev_class, MKDEV(Major, 0));
  chardev_created = false;
}


// Callback used when the USB device matching `vendor` and `product` ids is connected.
static int device_connect(struct usb_interface *interface,
                          const struct usb_device_id *id)
{
  printk(KERN_INFO "stack_chardev: USB device connected: %04X:%04X\n", id->idVendor, id->idProduct);
  create_dev();
  my_usb_device = interface_to_usbdev(interface);
  device_present = true;
  return 0;
}

// Callback used when the USB device matching `vendor` and `product` ids is unplugged.
static void device_disconnect(struct usb_interface *interface)
{
  printk(KERN_INFO "stack_chardev: USB device disconnected\n");
  destroy_dev();
  printk(KERN_INFO "stack_chardev: chardev device destroyed\n");

  my_usb_device = NULL;
  device_present = false;
}

static struct usb_device_id my_usb_device_id[] = {
    {USB_DEVICE(VENDOR_ID, PRODUCT_ID)}, {} /* Terminating entry */
};

MODULE_DEVICE_TABLE(usb, my_usb_device_id);

static struct usb_driver my_usb_driver = {
    .name = "my_usb_driver",
    .id_table = my_usb_device_id,
    .probe = device_connect,
    .disconnect = device_disconnect,
};


static int __init chardev_init(void)
{
  struct usb_interface *intf;
  int interface_num = 0;
  int result;
  
  printk(KERN_INFO "stack_chardev: init\n");

  mutex_init(&my_mutex);

  Major = register_chrdev(0, DEVICE_NAME, &fops);
  if (Major < 0)
  {
    printk(KERN_ALERT "stack_chardev: failed to register a major number %d\n",
           Major);
    return Major;
  }

  stack = kmalloc(BUFFER_SIZE * sizeof(STACK), GFP_KERNEL);
  
  chardev_class = class_create(THIS_MODULE, "stack_chardev_class");
  usb_register_driver(&my_usb_driver, THIS_MODULE, "my_usb_driver");

  // Check for presence of USB device
  while ((intf = usb_find_interface(&my_usb_driver, interface_num++)) != NULL)
  {
    // while ((intf = usb_find_interface(&my_usb_driver, -1, my_usb_device_id)) != NULL) {
    if (device_present)
    {
      break;
    }
    result = usb_driver_claim_interface(&my_usb_driver, intf, THIS_MODULE);
    printk(KERN_INFO "stack_chardev: usb_driver_claim_interface result: %d\n",
           result);

    // Print the vendor and product IDs of the device
    printk(KERN_INFO "Vendor ID: %04X, Product ID: %04X\n",
           le16_to_cpu(my_usb_device->descriptor.idVendor),
           le16_to_cpu(my_usb_device->descriptor.idProduct));
  }

  if (device_present)
  {
    printk(KERN_INFO
           "stack_chardev: Device present, creating /dev/stack_chardev\n");
    create_dev();
  }

  return 0;
}

static void __exit chardev_exit(void)
{
  printk(KERN_INFO "stack_chardev: exit\n");

  mutex_lock(&my_mutex);
  // Destroy stack and release USB device
  destroy_dev();
  if (intf != NULL)
    usb_driver_release_interface(&my_usb_driver, intf);
  kfree(stack);
  class_unregister(chardev_class);
  class_destroy(chardev_class);
  unregister_chrdev(Major, DEVICE_NAME);
  usb_deregister(&my_usb_driver);
  mutex_unlock(&my_mutex);

  mutex_destroy(&my_mutex);

  printk(KERN_INFO "stack_chardev: unregistered successfully\n");
}

module_init(chardev_init);
module_exit(chardev_exit);
