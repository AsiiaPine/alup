#include <linux/module.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/mutex.h>
#include <linux/types.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Asiiapine");
MODULE_DESCRIPTION("A Simple chardev device implementing stack");

#define DEVICE_NAME "my_chardev"
#define BUFFER_SIZE 1024
#define CHANGE_STACK_SIZE 1

static int Major;

typedef struct {
  int32_t value;
} STACK;

static STACK *stack;
static ssize_t current_size = BUFFER_SIZE;
static ssize_t current_pos = -1;
static struct mutex my_mutex;


static int device_open(struct inode *inode, struct file *file)
{
    printk(KERN_INFO "my_chardev: device_open\n");
    try_module_get(THIS_MODULE);
    return 0;
}

static ssize_t device_read(struct file *filp, char *buffer, size_t length, loff_t *offset)
{
    mutex_lock(&my_mutex);
    int bytes_read = 0;
    
    printk(KERN_INFO "my_chardev: device_read\n");

    while (length >= sizeof(STACK) && current_pos >= 0) {
        if (copy_to_user(buffer, &stack[current_pos--], sizeof(STACK))) {
            mutex_unlock(&my_mutex);
            return -EFAULT;
        }
        length -= sizeof(STACK);
        buffer += sizeof(STACK);
        bytes_read += sizeof(STACK);
    }

    mutex_unlock(&my_mutex);

    return bytes_read;
}

static ssize_t device_write(struct file *filp, const char *buf, size_t len, loff_t *offset)
{
    printk(KERN_INFO "my_chardev: device_write\n");

    mutex_lock(&my_mutex);

    int bytes_written = 0;

    if ((current_pos + 1) * sizeof(STACK) >= current_size) {
        printk(KERN_ALERT "Stack is full\n");
        mutex_unlock(&my_mutex);
        return -ENOSPC;
    }
    
    while (len >= sizeof(STACK) && (current_pos + 1) * sizeof(STACK) < current_size) {
        if (copy_from_user(&stack[++current_pos], buf, sizeof(STACK))) {
            mutex_unlock(&my_mutex);
            return -ENOSPC;
        }
        buf += sizeof(STACK);
        len -= sizeof(STACK);
        bytes_written += sizeof(STACK);
    }

    mutex_unlock(&my_mutex);

    printk(KERN_INFO "Written %d bytes to stack\n", bytes_written);
    return bytes_written;
}

static int device_release(struct inode *inode, struct file *filp)
{
    printk(KERN_INFO "my_chardev: device_release\n");
    // Delete if not working
    if (try_module_get(THIS_MODULE))
        return 0;
    module_put(THIS_MODULE);

    return 0;
}

static long resize_stack(long new_size) {
    if (new_size <= 0 || new_size > 1024 * 1024 * 512)
      return -EINVAL;

    printk(KERN_INFO "my_chardev: resizing stack to %d bytes\n", new_size);

    mutex_lock(&my_mutex);
    STACK *new_stack = kvmalloc(new_size * sizeof(STACK), GFP_KERNEL);
    ssize_t size_in_bytes = new_size * sizeof(STACK);
    ssize_t bytes_to_copy = min(size_in_bytes, current_size * sizeof(STACK));
    memcpy(new_stack, stack, bytes_to_copy);
    kfree(stack);
    stack = new_stack;
    current_size = new_size;
    if (current_pos > current_size-1)
        current_pos = current_size-1;
    mutex_unlock(&my_mutex);
    return 0;
}

static long device_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
    printk(KERN_INFO "my_chardev: device_ioctl\n");

    switch (cmd)
    {
        case CHANGE_STACK_SIZE:
            return resize_stack((long) arg);
    }

    return 0;
}

static const struct file_operations fops = {
    .owner = THIS_MODULE,
    .open = device_open,
    .release = device_release,
    .write = device_write,
    .read = device_read,
    .unlocked_ioctl = device_ioctl
};

static int __init chardev_init(void)
{
    printk(KERN_INFO "my_chardev: init\n");

    mutex_init(&my_mutex);

    Major = register_chrdev(0, DEVICE_NAME, &fops);
    stack = kmalloc(BUFFER_SIZE * sizeof(STACK), GFP_KERNEL);

    if (Major < 0)
    {
        printk(KERN_ALERT "my_chardev: failed to register a major number %d\n", Major);
        return Major;
    }

    printk(KERN_INFO "I was assigned major number %d. To create\n", Major);
    printk(KERN_INFO "a stack, create a dev file with\n");
    printk(KERN_INFO "'mknod /dev/%s c %d 0'.\n", DEVICE_NAME, Major);

    return 0;
}

static void __exit chardev_exit(void)
{
    printk(KERN_INFO "my_chardev: exit\n");

    mutex_lock(&my_mutex);
    unregister_chrdev(Major, DEVICE_NAME);
    kfree(stack);
    mutex_unlock(&my_mutex);

    mutex_destroy(&my_mutex);

    printk(KERN_INFO "my_chardev: unregistered successfully\n");
}

module_init(chardev_init);
module_exit(chardev_exit);
