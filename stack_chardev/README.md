# My CHARDEV kernel module
(with stack data structure)

Chardev kernel module provides an interface for user-space programs to interact with a device. So, to create a module, a person has to implement basic functions (open, read, write, release).

## Description:
### Step 1: Create chardev .c file
Use simple data struct which is used for an interaction with device (file) via storing elements of 32 bits:
```
typedef struct {
  int32_t value;
} STACK;
```

Create a kernel module with the following functions:

- **open(): Initializes the device file when it is opened.**
Calls `try_module_get` to increment the module's usage counter.

```
static int device_open(struct inode *inode, struct file *file)
{
    printk(KERN_INFO "my_chardev: device_open\n");
    try_module_get(THIS_MODULE);
    return 0;
}
```

- **read(): Reads data from the device file and returns it to the user.**
When a thread calls the function, it locks the mutex exclusively for this task. If the mutex is not available right now, the thread will sleep until it can get it.
The function checks if the `copy_to_user` run successfully, pops the elements from the stack and copy them into process' buffer, otherwise some data will be missed during the operation, so the `ENOMEM` error has been added.

```
static ssize_t device_read(struct file *filp, char *buffer, size_t length, loff_t *offset)
{
    mutex_lock(&my_mutex);
    int bytes_read = 0;

    printk(KERN_INFO "my_chardev: device_read\n");

    while (length >= sizeof(STACK) && current_pos >= 0) {
        if (copy_to_user(buffer, &stack[current_pos--], sizeof(STACK))) {
            mutex_unlock(&my_mutex);
            return -ENOMEM;
        }
        length -= sizeof(STACK);
        buffer += sizeof(STACK);
        bytes_read += sizeof(STACK);
    }

    mutex_unlock(&my_mutex);

    return bytes_read;
}

```

- **write: Writes data to the device file.**

The function writes the data passed by a thread and checks if the stack can manage the data, if not, the `ENOSPC` is raised.
While the buf data is written into the stack, it splits the buf into 32 bits segments and pop them one by one. 
The write function guards the stack with `mutex_lock` and does not allow other threads to change the data of the stack during the operation.
```
static ssize_t device_write(struct file *filp, const char *buf, size_t len, loff_t *offset)
{
    printk(KERN_INFO "my_chardev: device_write\n");

    mutex_lock(&my_mutex);

    int bytes_written = 0;

    if ((current_pos + 1) * sizeof(STACK) >= current_size) {
        printk(KERN_ALERT "Stack is full\n");
        mutex_unlock(&my_mutex);
        return -ENOSPC;
    }

    while (len >= sizeof(STACK) && (current_pos + 1) * sizeof(STACK) < current_size) {
        if (copy_from_user(&stack[++current_pos], buf, sizeof(STACK))) {
            mutex_unlock(&my_mutex);
            return -ENOSPC;
        }
        buf += sizeof(STACK);
        len -= sizeof(STACK);
        bytes_written += sizeof(STACK);
    }

    mutex_unlock(&my_mutex);

    printk(KERN_INFO "Written %d bytes to stack\n", bytes_written);
    return bytes_written;
}
```
- ***release: Cleans up the device file when it is closed.***
The function calls `module_put`, which decrement the usage count of the module, when the value decrease to zero, the kernel may safely unload the `chardev` module.
```
static int device_release(struct inode *inode, struct file *filp)
{
    printk(KERN_INFO "my_chardev: device_release\n");
    module_put(THIS_MODULE);

    return 0;
}
```
- ***ioctl: An optional function for device configuration.***
The function is called when a process calls `IOCTL` command to device file. The `device_ioctl` changes the stack size to value passed with the command.

```
static long device_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
    printk(KERN_INFO "my_chardev: device_ioctl\n");

    switch (cmd)
    {
        case CHANGE_STACK_SIZE:
            return resize_stack((long) arg);
    }

    return 0;
}
```

- **resize_stack**
The function is used to create new stack with `new_size` and copy as many as possible elements from old stack. Old stack will be deleted by `kfree` call.
```
static long resize_stack(long new_size) {
    if (new_size <= 0 || new_size > 1024 * 1024 * 512)
      return -EINVAL;

    printk(KERN_INFO "my_chardev: resizing stack to %d bytes\n", new_size);

    mutex_lock(&my_mutex);
    STACK *new_stack = kvmalloc(new_size * sizeof(STACK), GFP_KERNEL);
    ssize_t size_in_bytes = new_size * sizeof(STACK);
    ssize_t bytes_to_copy = min(size_in_bytes, current_size * sizeof(STACK));
    memcpy(new_stack, stack, bytes_to_copy);
    kfree(stack);
    stack = new_stack;
    current_size = new_size;
    mutex_unlock(&my_mutex);
    return 0;
}
```

## Step 2: Compile the kernel module

A proper Makefile should be created.
The problem here is that the needed lib files are located inside the kernel folder, therefore we have to explicitly point on the built libraries used in the module.

Makefile:

```
obj-m := stack_chardev.o
KDIR := /lib/modules/$(shell uname -r)/build
PWD := $(shell pwd)

all:
 make -C $(KDIR) M=$(PWD) modules

clean:
 make -C $(KDIR) M=$(PWD) clean
```

The compilation!
Call `make` in the project directory to compile the project

```
make 
```
## Working with the module

Insert the module into kernel

```
sudo insmod stack_chardev.ko
```

Check if the module is loaded

```
lsmod | grep my_module
```

Call dmesg to find *Major* number assigned to the chardev:
```
sudo dmesg
```
Create a new character device file:
```
sudo mknod /dev/my_chardev c <Major> 0
```
Test the open, read, write and release functions:
Write line into stack 

```
echo "Hello World" | sudo tee /dev/my_chardev  
``` 

Read head *N* bytes of the stack:

```
sudo head -c <N> /dev/my_chardev   
```

Change size of stack to *N* int32_t's:

```
sudo ioctl /dev/my_chardev 1 -v <N>      
```
Result:
![](pictures/Screenshot_20230427_202331.png)
![](pictures/Screenshot_20230427_204346.png)
![](pictures/Change_stack_size.png)
Fixed bugs:
Incorrect pointer to the stack' head after resizing.
![](pictures/Broken_change_of_size.png)
